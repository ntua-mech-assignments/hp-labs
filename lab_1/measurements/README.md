# Measurements from 21-11-2023
These measurements refer to the circuit in its original form, which the Simscape model also refers to. In all measurment sets, the pressure was regualted with the valve after the pressure transducer, so the Simscape model does not need to include a variable resistance. Finally, the measurements with "closed" in their name refer to a state with no fluid flow and can be used to calibrate the pressure transducer.

Sampling frequency was 200 Hz, which prooved to be too low for frequency domain analysis.

# Measurements from 29-11-2023
These measurements were taken with the circuit in a different form. Now the pressure gauge and the transducer were separated only by a valve and 2-3 angled connections. We do not have a model for this circuit.  

Sampling frequency was on average 1000 Hz, but the samples came in batches of around 6.

# Measurements from 10-12-2023
This time we used the [LowLatencyLogger](https://github.com/greiman/NilRTOS-Arduino/blob/master/libraries/SdFat/examples/LowLatencyLogger/LowLatencyLogger.ino) Arduino sketch paired with and SD card and we were able to achieve a **stable** sampling frequency of 1 kHz. Mesurements were taken at around 860 RPM and around 15 bar of pressure.
Specific values of RPM and pressure were of no particular concent because the goal of this session was to test the LowLatencyLogger and get the shape of the pressure pulsation.