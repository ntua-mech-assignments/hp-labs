import numpy as np
import time
import csv
import serial
import pandas as pd
import matplotlib.pyplot as plt

filename = input("Enter filename: ")


# initialize serial port
ser = serial.Serial()
ser.port = '/dev/ttyACM0' #Arduino serial port
ser.baudrate = 57600
ser.open()
if ser.is_open==True:
	print("\nAll right, serial port now open. Configuration:\n")
	print(ser, "\n") #print serial parameters
	
times = []
pressures = []
start_time = time.time()

try:
    while True:
        line=ser.readline()      # ascii
        string_n = line.decode('utf-8','ignore')
        flt = string_n.rstrip()
        if flt != '' and flt != '0\r0':
            # flt = float(flt) # does not make any difference when we are writing to csv
            # print(flt)
            elapsed_time = (time.time() - start_time)

            times.append(elapsed_time)
            pressures.append(flt)
        

except KeyboardInterrupt:
    # Close the serial connection and plot on keyboard interrupt
    ser.close()
    print("Serial connection closed.")    
    dict = {"time":times, "pressure":pressures}
    df = pd.DataFrame(data=dict)

    df = df[df["time"]>2]
    df.to_csv(filename,index=False, header=False)

    diffs = np.zeros(df.shape[0]-1)

    # print("================================")
    for i in range(df.shape[0]-1):
    #     # print(i)
        # print(df["time"][i])
        diffs[i] = times[i+1] - times[i]
    
    print(diffs.max - diffs.min)
    print(1/np.mean(diffs))


