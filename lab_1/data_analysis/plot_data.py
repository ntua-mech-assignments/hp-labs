import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# from analog2bar import analog2bar

def analog2bar(analog): 
    return analog*0.06

nominal_pressure = 5 # on the pressure gauge, in bar
nominal_rpm = 700
# filename = "lab_1\\measurements\\21-11-2023\\%.0lfbar_%.0lfrpm.csv"%(nominal_pressure,nominal_rpm)

filename = "lab_1/measurements/adc4pin04.csv"
data = pd.read_csv(filename,names = ['time','pressure'])

times = np.array(data['time'], dtype = 'float64')
pressure = np.array(data['pressure'], dtype='float64')
mean_pressure = np.mean(analog2bar(data['pressure']))

print("Pressure loss for %lf bar and %lf rpm is : %lf"%(nominal_pressure, nominal_rpm, (nominal_pressure - mean_pressure)))

fig, ax  = plt.subplots()
ax.plot(data['time'],analog2bar(data['pressure']))
ax.plot(data['time'], mean_pressure*np.ones(data.shape[0]), linestyle="--", color="firebrick")
ax.plot(data['time'], nominal_pressure*np.ones(data.shape[0]), linestyle='--', color="firebrick")

ax.set_ylabel("Transducer pressure [bar]")
ax.set_xlabel("Time [sec]")

plt.show()

# =============== FREQUENCY DOMAIN ANALYSYS ================

diffs = np.zeros(data.shape[0]-1)
for i in range(data.shape[0]-1):
#     # print(i)
    # print(df["time"][i])
    diffs[i] = times[i+1] - times[i]

# print(np.max(diffs) - np.min(diffs))
print(1/np.mean(diffs))


# sampling_freq = 1/0.005

# f = pressure[int(2*sampling_freq):]
# print(f.shape[0])
# f = f - np.mean(f)
# fhat = np.fft.fft(f)
# N =  len(f)
# n = np.arange(N)
# freq = n / sampling_freq
# psd = fhat * np.conjugate(fhat)
# freq = np.fft.fftfreq(f.shape[0], 1/sampling_freq)


# # plt.stem(freq, np.abs(f), 'b', \
# #          markerfmt=" ", basefmt="-b")
# # plt.xlabel('Freq (Hz)')
# # plt.ylabel('FFT Amplitude |X(freq)|')


# plt.plot(freq,fhat)
# plt.show()
