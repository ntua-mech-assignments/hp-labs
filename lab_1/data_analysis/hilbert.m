clear 
clc 

% Load data
% data preparation
fs = 1000 ; 
Ts = 1/fs ; 
M = readmatrix('../measurements/10-12-2023/around_860_rpm.csv');
sz = size(M);
offset = M(1,1);
valuemean = mean(M(:,2));

% for i = 1:sz(1,1)
%     M(i,1) = M(i,1) - offset;
%     M(i,2) = M(i,2) - valuemean;
% end

unfiltered_data = M(:,2);
times = M(:,1);

%% Filter using low-pass Chebyshev filter
% unfiltered_data = unfiltered_data(4554:5437);
% Low-pass filtering with 200 Hz bandwidth 
fc = 200 ; 
% [filtered_data,d1] = lowpass(unfiltered_data,160,fs,ImpulseResponse="iir") ;
% pspectrum(filtered_data,fs)  % spectrum of the filtered data

[b,a] = cheby2(10,80,fc/(fs/2));
figure(1)
freqz(b,a,[],fs)

filtered_data = filtfilt(b,a,unfiltered_data); 
%% Filter using band pass filter
fpass = [100,200];
[filtered_data,d] = bandpass(unfiltered_data,fpass, fs); 
%% Plot filtered vs unfiltered data
figure(2)
plot(times,filtered_data);

xlabel("Time [us]")


