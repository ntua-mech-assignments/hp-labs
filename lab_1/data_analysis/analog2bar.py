import numpy as np

def analog2bar(analog):
    # Analog can either be a number or a np array 
    # The following function resulted from sensor calibration with the
    # measurement set of 21-11-2023

    # Simple (linear) sensor calibration : Maps ADC value to bar
    adc = [87.49, 160.18, 331.55] # mean values from 29-11-2023
    bar = [5, 10, 20] # manometer values

    nominal_coeffs = [0.05859, 0] # a,b for y=ax+b according to datasheet, if supply is 5V

    measured_coeffs = np.polyfit(adc, bar, 1)
    print("Measured calibration curve : P = %lf * ADC %lf"%(measured_coeffs[0],measured_coeffs[1]))

    return measured_coeffs[0] * analog + measured_coeffs[1]