clear 
clc 

% Load data
% data preparation
fs = 1000 ; 
Ts = 1/fs ; 
M = readmatrix('../measurements/10-12-2023/10bar_around_860_rpm.csv');
sz = size(M);
offset = M(1,1);
valuemean = mean(M(:,2));

for i = 1:sz(1,1)
%     M(i,1) = M(i,1) - offset;
    M(i,2) = M(i,2) - valuemean;
end

unfiltered_data = M(:,2);
filtered_data = bandpass(unfiltered_data, [100, 260], fs);
times = M(:,1);     

%% fft
figure(1);
% frequency plot line preparation
F1(sz(1,1),1) = zeros;
for i = 1:sz(1,1)
    F1(i,1) = fs*i/sz(1,1);
end
% fft
F2 = abs(fft(filtered_data));

for i = 1:sz(1,1)
    if F1(i)>(fs/2)
        endpoint = i;
        break
    end
end
plot(F1(1:endpoint),F2(1:endpoint));
xlabel("Hz")
ylabel("Amplitude")

%% time-domain plot
figure(2);
plot(times, 0.061*filtered_data -0.09);
xlabel("Time [us]")
ylabel("Pressure [bar]")
title("Bandpass-filtered signal in time domain")