# Ventouri flow meter 
Some instructions on how to manufacture and use : 
* Adjust the measuring tube holes based on the diameter of your tubes. Make the small hole such that it allows for tube misallignment and make the diameter of the counterbore big enough that an adequate quantity of adhersive can be applied. 
* Make the pipe hole a bit smaller than the nominal pipe diameter. Use epoxy and press-fit the pipes into the 3D printed part
* We printed it in PETG with 4 perimeters. One could also use the techniques described [here](https://help.prusa3d.com/article/watertight-prints_112324?_gl=1*1c23w5y*_ga*MzI4MjM0OTg1LjE3MDI0MDYwNjk.*_ga_3HK7B7RT5V*MTcwMjQwNjA2OC4xLjAuMTcwMjQwNjA2OC42MC4wLjA)
* Under no circumstances do not put pressure in the system. At the very least you will get covered in oil - in the worst case the 3DP part with fail.