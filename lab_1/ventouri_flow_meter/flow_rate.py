import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

def fluid_col_to_pa(rho, fluid_col):
    return fluid_col * 9.81 * rho

def flow_rate(P_diff, rho, A1, A2):
    # A1, A2 in m^2 
    # Pdiff in Pa
    # rho in kg/m^3
    zeta_taper = 0.045 # -- approximate coefficient from fluid dynamics book 

    dP_bernoulli = rho/2 * 1/A1**2 * (A1**2/A2**2 - 1)
    dP_taper = zeta_taper * rho / 2 / A2**2 
    Q =  np.sqrt(P_diff / (dP_bernoulli + dP_taper))

    # Q = A1 * np.sqrt(2/rho * P_diff/(A1**2/A2**2 -1))
    return Q

def m3s_to_lmin(Q):
    Q_new = Q * 60e3
    return Q_new

def m3s_to_cm3s(Q): 
    Q_new = Q * 1e6 
    return Q_new

g = 9.81 
D2 = 7e-3 
A2 = np.pi * D2**2 / 4 
D1 = 11.5e-3 
A1 = np.pi * D1**2 / 4

rho = 900 # kg/m^3

RPM = np.array([530, 640, 700, 867])
P_diffs = np.array([10.1e-2, 14.4e-2, 16.2e-2, 23.5e-2])  # in m of oil column
P_diffs = fluid_col_to_pa(rho, P_diffs) # in Pa


Q = flow_rate(P_diffs, rho, A1, A2)
Q_lpm = Q * 60e3

print("Q in l/min : ")
print(Q_lpm)

coeffs = np.polyfit(RPM, Q_lpm, 1)
plt.scatter(RPM, Q_lpm, s=5.0, c='r')
plt.plot(RPM, coeffs[0]*RPM + coeffs[1])

print("slope = %lf [l/min / RPM]"%coeffs[0])
print("Y intercept : %lf [l/min] at 0 RPM"%coeffs[1])
print("Flow rate in cm^3/rev = %lf"%(coeffs[0]*1000))
corr, _ = pearsonr(RPM, P_diffs)
print("Correlation coefficient of the measurement is : %lf"%corr)

plt.xlabel("RPM")
plt.ylabel("Flow rate [cm^3/sec]")
plt.show()