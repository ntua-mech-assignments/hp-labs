import numpy as np

## Input

Q = 15*1.6667e-5 # m3/s
D1 = 11.5e-3 #m
rho = 900 # kg/m3
# Calculate Dp
Dp = 200 # Pa
#print(Dp/100000*1000)
D2 = np.sqrt(4*np.pi*np.sqrt((np.pi*(D1/2)**2)**2/(2*Dp*np.pi*(D1/2)**2/(rho*Q**2) + 1)))

print(D2*1000) # mm
