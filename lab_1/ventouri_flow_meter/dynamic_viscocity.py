import numpy as np 

D_ball = 5.95e-3 # m 
R_ball = D_ball /2 
V_ball = 4/3 * np.pi * R_ball**3

m_ball = 0.2e-3 # kg 
rho_ball = m_ball / V_ball 

rho_oil = 900 # kg/m^3 -- measured
g = 9.81 

ball_velocity_mpf = 0.01/20 # measured velocity in meters per frame
fps = 240 
ball_velocity = ball_velocity_mpf * fps # m/sec -- measured 

dyn_visc = 2*(rho_ball - rho_oil) * g * R_ball**2 / (9*ball_velocity)
kin_visc = dyn_visc/rho_oil

print("Dynamic viscocity : %lf [Pa*s]"%dyn_visc)
print("Kinematic viscocity : %lf [m^2/s]"%kin_visc)
