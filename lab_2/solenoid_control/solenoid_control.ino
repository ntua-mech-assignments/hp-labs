String userInput = "s";
int enA = 8; 
int enB = 7; 
int Ap = 11 ; 
int Am = 10 ;
int Bp = 6 ; 
int Bm = 5 ; 

void setup() {
  // Start serial communication
  Serial.begin(9600);
  
  // Set constant polarity be setting the minus driver pins constanly low
  digitalWrite(Am, LOW);
  digitalWrite(Bm, LOW);

  // start with all solenoids closed for safety 
  digitalWrite(Ap, LOW); 
  digitalWrite(Bp, LOW);

  // Initial message
  Serial.println("Enter a command:");
  Serial.println("A/a -> Actuate port A");
  Serial.println("B/b -> Actuate port B");
  Serial.println("S/s -> Restore directional valve to neutral position");
}

void loop() {
  // Check if data is available to read from the serial buffer
  if (Serial.available() > 0) {
    // Read the input message from the serial buffer
    String userInput = Serial.readStringUntil('\n');

    // Print the received message
    Serial.print("Received: ");
    Serial.println(userInput);

    // Print the original message again
    Serial.println("Enter a command:");
    Serial.println("A/a -> Actuate port A");
    Serial.println("B/b -> Actuate port B");
    Serial.println("S/s -> Restore directional valve to neutral position");

    // Clear the input buffer
    while (Serial.available() > 0) {
      Serial.read();
    }
  }

  if (userInput = "a" || userInput == "A"){
    // actuate solenoid A and close B
    digitalWrite(Ap, HIGH); 
    digitalWrite(Bp, LOW);
    Serial.println("A is PRESSED"); 
  } 

  if (userInput = "b" || userInput == "B"){
    // actuate solenoid B and close A
    digitalWrite(Ap, LOW); 
    digitalWrite(Bp, HIGH);
    Serial.println("B is PRESSED"); 
  } 

  if (userInput = "s" || userInput == "S"){
    // close both solenoids
    digitalWrite(Ap, LOW); 
    digitalWrite(Bp, LOW);
    Serial.println("S is PRESSED");
  } 

delay(300); 

}